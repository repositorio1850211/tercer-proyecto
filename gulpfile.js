import path from 'path'
import fs from 'fs'
import { glob } from 'glob'
import { src, dest, watch, series } from 'gulp'
import * as dartSass from 'sass'
import gulpSass from 'gulp-sass'
import concat from 'gulp-concat'
import rename from 'gulp-rename'

const sass = gulpSass(dartSass)

import terser from 'gulp-terser';
import sharp  from 'sharp'

export function js(done){
    src("src/js/**/*.js")
        .pipe(concat('bundle.js')) // Une todos los archivos js de la carpeta en uno solo con el nombre de bundle
        .pipe(terser()) // Agrupa todo el codigo en una sola linea
        .pipe(rename({ suffix: '.min' })) // le agrga .min al archivo js bundle para que quede bundle.min.js
        .pipe(dest('./build/js'))
    done()
}

export function css( done ) {
    
    src('src/scss/app.scss', {sourcemaps:true})
        .pipe( sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError) )
        .pipe( dest('build/css'), {sourcemaps:'.'} )

    done()// callback que avisa a gulp cuando llegamos al final
}

export async function crop(done) {
    const inputFolder = 'src/img/' //Busca la galeria de imagenes
    const outputFolder = 'src/img/thumb'; // crea una carpeta donde guardar las imagenes pequeñas
    const width = 250;
    const height = 180;
    if (!fs.existsSync(outputFolder)) {
        fs.mkdirSync(outputFolder, { recursive: true })
    }
    const images = fs.readdirSync(inputFolder).filter(file => {
        return /\.(jpg)$/i.test(path.extname(file));
    });
    try {
        images.forEach(file => {
            const inputFile = path.join(inputFolder, file)
            const outputFile = path.join(outputFolder, file)
            sharp(inputFile) 
                .resize(width, height, {
                    position: 'centre'
                })
                .toFile(outputFile)
        });

        done()
    } catch (error) {
        console.log(error)
    }
}

// Convertidor de imagenes a WebP

export async function imagenes(done) {
    const srcDir = './src/img';
    const buildDir = './build/img';
    const images =  await glob('./src/img/**/*{jpg,png}')

    images.forEach(file => {
        const relativePath = path.relative(srcDir, path.dirname(file));
        const outputSubDir = path.join(buildDir, relativePath);
        procesarImagenes(file, outputSubDir);
    });
    done();
}

function procesarImagenes(file, outputSubDir) {
    if (!fs.existsSync(outputSubDir)) {
        fs.mkdirSync(outputSubDir, { recursive: true })
    }
    const baseName = path.basename(file, path.extname(file))
    const extName = path.extname(file)
    const outputFile = path.join(outputSubDir, `${baseName}${extName}`)
    const outputFileWebp = path.join(outputSubDir, `${baseName}.webp`)
    const outputFileAvif = path.join(outputSubDir, `${baseName}.avif`)

    const options = { quality: 80 }
    sharp(file).jpeg(options).toFile(outputFile)
    sharp(file).webp(options).toFile(outputFileWebp)
    sharp(file).avif().toFile(outputFileAvif)
}

export function dev(){
    watch('src/scss/**/*.scss', css)
    watch('src/js/**/*.js', js)
    watch('src/img/**/*.{png, jpg}', imagenes)
}

export default series( crop, js, css, imagenes, dev)